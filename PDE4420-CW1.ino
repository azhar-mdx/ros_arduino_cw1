// Including headers
#include <ros.h>
#include <std_msgs/Int16.h>

// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the LDR sensor is attached to
const int LEDs[] = {2, 3}; // Output pins that the LEDs are attached to

// Creating node handler
ros::NodeHandle nh;


// Callback function for subscriber
void LDRmessage( const std_msgs::Int16& value ){
  
  // variable for holding values
  int OutputValue = 0;

  // map it to the range of the analog out:
  OutputValue = map(value.data, 0, 300, 0, 1);
  OutputValue = map(OutputValue, 0, 1, 225, 0);

  // Negative values were observed during testing.
  // Therefore, adding a precaution to eleminate output errors.
  if(OutputValue < 0){ OutputValue = 0; }
  
  // Change state of all LEDs:
  for (int i = 0; i < 2; i = i + 1) {
    analogWrite(LEDs[i], OutputValue);
  }
  
}

std_msgs::Int16 LDRValue; // Defining messages
ros::Subscriber<std_msgs::Int16> s("LDR", LDRmessage); // Instantiate the subscriber
ros::Publisher p("LDR", &LDRValue); // Instantiate the publisher

// Using Arduino setup function to initialize ros node handler and advertiser
void setup() {
  nh.initNode();
  nh.advertise(p);
  nh.subscribe(s);
}

// Using a loop function
void loop() {
  LDRValue.data = analogRead(analogInPin); // Getting data from LDR sensor
  p.publish( &LDRValue ); // Passing data to publish
  nh.spinOnce(); // Publishing the LDR data using spinOnce function
  delay(50); // Using delay for stability
}
