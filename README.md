**PDE4420-CW1**

## Purpose of this project

This project is developed using ros and arduino for course work 1 of PDE4420 module. The project utilizes microcontrollers, sensors and rosserial to publish and subscribe to data. The data is then used in logic to provide a meaningful output using some microcontrollers.

---

## Resources of this project

This project is devloped using following resources:

01. One Arduino Nano
02. One breadboard
03. One LDR sensor
04. Two LED lights
06. Few wires and connectors
07. Arduino IDE
08. A personal computer with Linux OS
09. Ros
10. Rosserial
11. Internet

---

## Scope of this project

The scope of this project is to present a simulation of automatically switching lights on and off. This simulation was created while having following thoughts in mind:
01. This can be applied to street/road lights.
02. This can be used in autonomous robotos and vehicles.
03. This can be used in homes, specially in DOMs.

This project can also be called as green IT.

---

## How the project works

The logic is pretty straight forward. When the LDR sensor is exposed to light, meaning that there is light available in the area, then it will switch off the LEDs, and vice versa.

---

## How to setup this project on your machine

01. Clone this directory on your machine using: git clone directory_name
02. Make sure your arduino board is connected properly.
03. Upload the arduino program on your board.
04. Make sure LEDs are connect to port D2, D3 and LDR sensor is connected to A0.
04. Open terminal, cd project_directory and use following command: ./launch.sh

---
